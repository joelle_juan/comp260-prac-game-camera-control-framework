﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour {

    Camera cam;

    public void Start()
    {
        cam = GetComponent<Camera>();
    }

    public void Update()
    {
        if (Input.GetKey(KeyCode.KeypadPlus))
            cam.orthographicSize -= .1f;
        if (Input.GetKey(KeyCode.KeypadMinus))
            cam.orthographicSize += .1f;
    }
}
